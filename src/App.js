import React, {Component} from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import AllCryptoValues from './components/cryptoValue/allCryptoValues';
import AddNewCryptoValue from './components/cryptoValue/addNewCryptoValue';



class App extends Component {

    render() {
        return (
            <BrowserRouter>
                <div className="pixelplex-app">
                    <Switch>
                        <Route exact path='/addNewValue' component={AddNewCryptoValue}/>
                        <Route exact path='/' component={AllCryptoValues}/>
                    </Switch>
                </div>
            </BrowserRouter>
        );
    }
}


export default App;
