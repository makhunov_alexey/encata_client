import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addNewValue } from '../../store/actions/cryptoValueActions';

class AddNewCryptoValue extends Component {
    handleSubmit = (e) => {
        const personID = localStorage.getItem('id');
        if(personID){
            localStorage.setItem('id',personID);
            e.preventDefault();
            this.props.addNewValue({
                symbol:this.form.symbol.value,
                value:this.form.value.value,
                time: new Date(),
                personID: personID
            });
        } else {
            const newpersonID = this.guidGenerator();
            console.log(newpersonID);
            localStorage.setItem('id',newpersonID);
            e.preventDefault();
            this.props.addNewValue({
                symbol:this.form.symbol.value,
                value:this.form.value.value,
                time: new Date(),
                personID: newpersonID
            });
        }
        this.props.history.push('/');
    };

    guidGenerator = () => {
        var S4 = function() {
           return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
        };
        return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
    }
    
    render() {
        return (
            <div className="container">
                <h5>Create:</h5>
                <form onSubmit={this.handleSubmit} ref={el => (this.form = el)}>
                    <div className="form-group">
                        <label htmlFor="symbol">Symbol:</label>
                        <input type="text" className="form-control" name="symbol" id="Symbol" placeholder="Enter Symbol" ></input>
                    </div>
                    <div className="form-group">
                        <label htmlFor="value">Value:</label>
                        <input type="text" className="form-control" name="value" id="Value"  required></input>
                    </div>
                    <div className="input-field">
                        <button className="btn btn-outline-dark">Add</button>
                    </div>
                </form>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addNewValue: (information) => dispatch(addNewValue(information)),
    }
};

export default connect(null, mapDispatchToProps)(AddNewCryptoValue);
