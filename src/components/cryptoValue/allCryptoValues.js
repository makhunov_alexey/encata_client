import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getInformationAboutCryptoInDB, getInformationFromServer } from '../../store/actions/cryptoValueActions';

class AllCryptoValue extends Component {

    getInformation = ()=>{
        this.props.getInformationAboutCryptoInDB();
    }

    viewCryptoInformation = ()=>{
        this.props.getInformationFromServer();
    }

    render() {
        const {cryptoValues} = this.props;
        return (
            <div className="container">
            <button onClick={this.getInformation}className="btn btn-outline-dark">Get Information About Crypto cost</button>
            <button onClick={this.viewCryptoInformation}className="btn btn-outline-dark">View new crypto information</button>
            <a href='/addNewValue' className="btn btn-outline-dark">Add new crypto cost</a>
            <table className="table table-striped">
                <thead>
                    <tr>
                    <th>symbols</th>
                    <th>values</th>
                    <th>times</th>
                    <th>person id</th>
                    </tr>
                </thead>
                <tbody>
                    {cryptoValues && cryptoValues.map(element => {
                        return(
                        <tr>
                        <td>{element.symbol}</td>
                        <td>{element.value}</td>
                        <td>{element.time}</td>
                        <td>{element.personID}</td>
                        </tr>)
                    })}
                </tbody>
                </table>
            </div>
        );
    }
}

const mapStateToProps = (state) => {

    return {
        cryptoValues: state.cryptoValue.cryptoValues
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getInformationAboutCryptoInDB: () => dispatch(getInformationAboutCryptoInDB()),
        getInformationFromServer: () => dispatch(getInformationFromServer())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AllCryptoValue);
