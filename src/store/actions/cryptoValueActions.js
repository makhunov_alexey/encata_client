import axios from "axios";

const serverUrl = 'http://localhost:3000'

export const getInformationAboutCryptoInDB = () => {
    return async () => {
        try {
            await axios.get(`${serverUrl}/getCryptoInformations`);
        } catch (error) {
            console.log(error)
        }
    }
};

export const addNewValue = (information) => {
    return async () => {
        try {
            await axios.post(`${serverUrl}/createNewCrypto`, information);
        } catch (error) {
            console.log(error)
        }
    }
};

export const getInformationFromServer = () => {
    return async (dispatch) => {
        try {
            const response = await axios.get(`${serverUrl}/getValueOfCrypto`);
            console.log(response);
            dispatch({type: 'GET_INFORMATION', response});
        } catch (error) {
            dispatch({type: 'GET_INFORMATION_ERROR'});
        }
    }
};