const initState = {
    cryptoValues:[]
};

const articleReducer = (state = initState, action) => {

    switch (action.type) {
        case 'GET_INFORMATION':
            return {
                ...state,
                cryptoValues: action.response.data
            };
        case 'GET_INFORMATION_ERROR':
            return state;
        default:
            return state;
    }
};

export default articleReducer;