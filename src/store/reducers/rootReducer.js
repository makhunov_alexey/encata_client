import { combineReducers } from 'redux';
import cryptoValueReducer from './cryptoValueReducer'


const rootReducer = combineReducers({
    cryptoValue: cryptoValueReducer
});


export default rootReducer;